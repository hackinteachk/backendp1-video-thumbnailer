"""
Listen to Controller and generate
GIF images from given video bucket.
"""

from os import getenv, remove, path
from Rabbit import Rabbit
from json import loads
from collections import namedtuple
from requests import put, post, get, delete
from misc import check_sum, generate_thumbnail

HOST = getenv("MQ_HOST", "localhost")
USERNAME = getenv("MQ_USERNAME", "nut")
PASSWORD = getenv("MQ_PASSWORD", "nut")
KEYS = ['v_host', 'v_bucket', 'v_object',
        'g_host', 'g_bucket', 'g_object']
KEYS_ALL = ['v_host', 'v_bucket',
            'g_host', 'g_bucket']

INFO_TUPLE = namedtuple('once', KEYS)
INFO_TUPLE_ALL = namedtuple('info', KEYS_ALL)

rabbit = Rabbit(HOST, USERNAME, PASSWORD)


def callback(ch, method, properties, body):
    b = loads(body, encoding='UTF-8')
    print(f"==== Received event for object {b['v_object']} ====")
    # for k, v in b.items():
    #     print(f" [x] body[{k}] = {v}")

    ch.basic_ack(delivery_tag = method.delivery_tag)
    work(b)


def work(data):
    info = INFO_TUPLE(**data)
    object_runner(info, info.v_object)
    print(f"==== Finished generating file for object {info.v_object} ====")



def object_runner(info, v_object):
    print(f" **** Running {v_object} ****")
    fn, _ = path.splitext(v_object)
    g_object = fn + ".gif"
    object_get = get(f"http://{info.v_host}/{info.v_bucket}/{v_object}")

    # Write file
    with open(f"temp_files/{v_object}", "wb") as file:
        print(f" [x] Downloading {v_object}")
        for chunk in object_get.iter_content(chunk_size=256):
            file.write(chunk)
        print(f" [x] Saved {v_object}")

    # Generate thumbnail
    status = generate_thumbnail(v_object)
    err = f"Completed {g_object}" if status == 0 \
        else f"Error during generating {g_object}"
    print(f" [x] {err}")

    # Remove video file
    print(f" [x] Removing {v_object}")
    remove(f"temp_files/{v_object}")

    # === Uploading GIF to bucket
    print(f" [x] Uploading {g_object} to {info.g_host}/{info.g_bucket}")

    # Create bucket if not exist
    g_bucket_get = get(f"http://{info.g_host}/{info.g_bucket}?list")

    if g_bucket_get.status_code == 400:
        post(f"http://{info.g_host}/{info.g_bucket}?create")

    # Delete existing object if exists
    delete(f"http://{info.g_host}/{info.g_bucket}/{g_object}?delete")

    # Create target object
    post(f"http://{info.g_host}/{info.g_bucket}/{g_object}?create")

    # Upload part
    content_md5 = check_sum(f"output/{g_object}")
    with open(f"output/{g_object}", "rb") as file:
        put(f"http://{info.g_host}/{info.g_bucket}/{g_object}?partNumber=1",
            data=file,
            headers={
                "Content-MD5": content_md5
            })
    post(f"http://{info.g_host}/{info.g_bucket}/{g_object}?complete")
    print(f" [x] Uploaded {g_object}")
    # Remove gif files
    remove(f"output/{g_object}")
    print(f" [x] Removed temp file {g_object}")
    print(f" **** Done {v_object} ****")


rabbit.subscribe(callback)