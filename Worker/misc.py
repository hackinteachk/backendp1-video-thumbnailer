from hashlib import md5
from subprocess import call, PIPE
from functools import partial
from os import path

def generate_thumbnail(video_fn):
    """
    Generate thumbnail from
    videos in the directory
    `temp_files/`
    :return: exit code of calling make_thumbnail
    """
    fn, _ = path.splitext(video_fn)
    video_file = f"temp_files/{video_fn}"
    gif_output = f"output/{fn}.gif"
    cmd = f"make_thumbnail {video_file} {gif_output}"
    code = call(cmd, shell=True, stdout=PIPE, stderr=PIPE)
    return code


def check_sum(file):
    """
    :param file: filename to be check
    :return: checksum of the file
    """
    hash_md5 = md5()
    with open(file, 'rb') as gif:

        for chunk in iter(partial(gif.read, 128), b''):
            hash_md5.update(chunk)
    ret = hash_md5.hexdigest()
    print(f" [x] MD5: {ret}")
    return ret
