#!/bin/sh

num_worker=$1

if [ "$#" -ne 1 ]; then
    echo "Usage: ./start.sh <num_worker>"
    exit 1
fi

docker-compose up -d --build --scale worker=${num_worker}
