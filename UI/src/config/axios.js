/* eslint-disable import/prefer-default-export */
import axios from 'axios';

const URL = process.env.FLASK ? process.env.FLASK : 'http://localhost:5000/';
const videoHost = 'sos:3000';
const SOS = process.env.SOS ? process.env.SOS : 'http://localhost:3000/';

const api = axios.create({
  baseURL: URL,
  timeout: 1000,
});

const sos = axios.create({
  baseURL: SOS,
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json',
  },
});

export function getVideoList(bucketName) {
  return api.post(`/list_object/${bucketName}`, {
    v_host: videoHost,
  })
    .then(({ data }) => data);
}

export function once(bucketName, objectName) {
  return api.post(`/submit_once/${bucketName}/${objectName}`,
    {
      v_host: videoHost,
      g_host: videoHost,
      v_bucket: bucketName,
      g_bucket: bucketName,
      g_object: objectName,
    })
    .then(data => data);
}

export function all(bucketName) {
  return api.post(`/submit_all/${bucketName}`,
    {
      g_host: videoHost,
      v_host: videoHost,
      g_bucket: bucketName,
    })
    .then(data => data);
}

export function getImages(buckectName) {
  return api.post(`/list_images/${buckectName}`, {
    v_host: videoHost,
  })
    .then(({ data }) => data);
}

export function removeImage(link) {
  return sos.delete(`${link}`)
    .then(({ status }) => status);
}
