import Vue from 'vue';
import Router from 'vue-router';
import UI from '@/components/UI';


Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/:bucket_name',
      name: 'View',
      component: UI,
    },
  ],
});
