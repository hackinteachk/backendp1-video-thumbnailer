# Simple Storage - Thumnail Genarator

***Nuttapat Koonarangsri*** - webmaster@hackinteach.com

---

## Run

To start all services, simply run

```bash
# number of thumbnail worker to be scaled
$ ./start.sh <num_worker>
```



## Containers Brief Description

Services:port:

- **flask**:5000 => Web Controller for UI
- **mongodb** :27071 => Database for SOS
- **sos** :3000 => Simple Storage container
- **rab_mq** :15672 | 5672 => Rabbit MQ container
- **worker**  => Worker of Thumbnail generator, subscribed to rab_mq
- **ui** :8080 => Web UI for generate, view and delete GIF images in the bucket



## Accessing services

Access UI from your browser from 

```
localhost:8080/#/<bucket_name>
```

Example : 

```
localhost:8080/#/b1
```

![example](/Users/hackinteachk./Desktop/T1 2019/backend/assn/p1/example.png)

## Language

- **SOS** : golang
- **Worker** : python
- **Flask** (Web Controller) : python
- **UI** : VuetifyJS