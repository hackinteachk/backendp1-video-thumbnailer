from requests import get
from os import path


def list_videos(host, bucket):
    ret = get(f"http://{host}/{bucket}?list")
    if ret.status_code == 400:
        return []
    body = ret.json()
    objects = list((o["name"] for o in list(body["objects"])))
    videos = filter(lambda o: any(ext in get_ext(o) for ext in ["mov", "avi", "mp4"]), objects)
    return list(videos)


def get_ext(fn):
    _,ext = path.splitext(fn)
    return ext


def list_gif(host,bucket):
    ret = get(f"http://{host}/{bucket}?list")
    if ret.status_code == 400:
        return []
    body = ret.json()
    objects = list((o["name"] for o in list(body["objects"])))
    gifs = filter(lambda o: 'gif' in get_ext(o), objects)
    return list(gifs)