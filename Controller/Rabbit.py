"""
RabbitMQ Wrapper for P1
"""
import pika
import json

class Rabbit:
    """
    Main class for RabbitMQ
    """

    def __init__(self, host, username, password):
        self.queue_name = 'THUMBNAILER'
        self.host = host
        self.username = username
        self.password = password
        self.cred = pika.PlainCredentials(
            username=self.username, password=self.password)
        self.conn_param = pika.ConnectionParameters(
            host=self.host, credentials=self.cred)

    def connect(self):
        """
        :return: RabbitMQ Connection
        """
        return pika.BlockingConnection(self.conn_param)

    def emit(self, data):
        """
        :param data: message (or dictionary) to be sent
        :return: True if message emitted succesfully, otherwise, False
        """
        conn = self.connect()
        channel = conn.channel()
        channel.queue_declare(queue=self.queue_name, durable=True)
        ret = channel.basic_publish(exchange='',
                                    routing_key=self.queue_name,
                                    body=json.dumps(data),
                                    properties=pika.BasicProperties(
                                        delivery_mode=2,  # make message persistent
                                    ))
        conn.close()
        return ret

    def subscribe(self, cb):
        """
        :param cb: callback function to be called when subscribe
        :return: ...
        """
        print(f"Connecting to {self.host}")
        conn = self.connect()
        print("Connected")
        channel = conn.channel()
        channel.queue_declare(queue=self.queue_name, durable=True)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(consumer_callback=cb, queue=self.queue_name)
        try:
            channel.start_consuming()
        except RecursionError:
            print("Error during subscribe")
