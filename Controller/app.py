import logging
import os
from Rabbit import Rabbit
from flask import Flask, jsonify, request
from flask_api import status
from misc import list_videos, get_ext, list_gif
from requests import get, delete
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = '*'
logging.basicConfig(level=logging.DEBUG)

HOST = os.getenv('MQ_HOST', 'localhost')
USERNAME = os.getenv('MQ_USERNAME', 'guest')
PASSWORD = os.getenv('MQ_PASSWORD', 'guest')

KEYS = ['v_host', 'v_bucket', 'v_object',
        'g_host', 'g_bucket', 'g_object']
KEYS_ALL = ['v_host', 'v_bucket',
            'g_host', 'g_bucket']


@app.route('/submit_all/<string:bucket_name>', methods=["POST"])
def submit_all(bucket_name):
    data = request.json
    data['v_bucket'] = bucket_name
    if not all(k in data for k in KEYS_ALL):
        ret = {"error": f"Please specify the following data : {' '.join(KEYS_ALL)}"}
        return jsonify(ret), status.HTTP_400_BAD_REQUEST

    # Get bucket name
    get_bucket = get(f"http://{data['v_host']}/{data['v_bucket']}?list")

    if get_bucket.status_code == 400:
        app.logger.debug(f" [x] ERROR : bucket {data['v_bucket']} not found")
        ret = {"error": f"{bucket_name} not found"}
        return jsonify(ret), status.HTTP_400_BAD_REQUEST

    body = get_bucket.json()
    objects = list((o["name"] for o in list(body["objects"])))

    rabbit = Rabbit(HOST, USERNAME, PASSWORD)

    for v_object in objects:
        if any(ext in get_ext(v_object) for ext in ["mov", "avi", "mp4"]):
            data['v_object'] = v_object
            fn, _ = os.path.splitext(v_object)
            data['g_object'] = fn+".gif"
            ret = rabbit.emit(data)

    return str(ret), status.HTTP_200_OK


@app.route('/submit_once/<string:bucket_name>/<string:object_name>', methods=["POST"])
def submit_once(bucket_name, object_name):
    data = request.json
    app.logger.debug(data)
    data['v_bucket'] = bucket_name
    data['v_object'] = object_name
    data['g_object'] = object_name + ".gif" if ".gif" not in object_name else object_name

    if not all(k in data for k in KEYS):
        ret = {"error": f"Please specify the following data : {' '.join(KEYS)}"}
        return jsonify(ret), status.HTTP_400_BAD_REQUEST

    get_bucket = get(f"http://{data['v_host']}/{data['v_bucket']}?list")

    if get_bucket.status_code == 400:
        print(f" [x] ERROR : bucket {data['v_bucket']} not found")
        return

    body = get_bucket.json()
    objects = list((o["name"] for o in list(body["objects"])))
    if object_name not in objects:
        ret = {"error": f"Please specify the following data : {' '.join(KEYS)}"}
        return jsonify(ret), status.HTTP_400_BAD_REQUEST
    rabbit = Rabbit(HOST, USERNAME, PASSWORD)
    ret = rabbit.emit(data)
    return str(ret), status.HTTP_200_OK


@app.route('/list_object/<string:bucket>', methods=["POST"])
def list_bucket(bucket):
    data = request.json
    app.logger.debug(data)
    ret = {
        "objects": list_videos(data['v_host'], bucket)
    }
    return jsonify(ret), status.HTTP_200_OK


@app.route('/list_images/<string:bucket>', methods=["POST"])
def list_img(bucket):
    data = request.json
    ret = {
        "objects": list_gif(data['v_host'],bucket)
    }
    return jsonify(ret), status.HTTP_200_OK


if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

if __name__ == '__main__':
    app.run(debug=True, use_reloader=False)